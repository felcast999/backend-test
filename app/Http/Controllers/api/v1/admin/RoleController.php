<?php

namespace App\Http\Controllers\Api\V1\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Datatables\Role\RoleDataTable;

use App\Http\Requests\V1\Role\CreateRequest;
use App\Http\Requests\V1\Role\UpdateRequest;
use App\Http\Requests\V1\Role\DestroyRequest;

use App\UseCases\V1\Role\Create;
use App\UseCases\V1\Role\Update;
use App\UseCases\V1\Role\Show;
use App\UseCases\V1\Role\Destroy;


class RoleController extends Controller
{
       public function __construct()
    {
       $this->middleware('role:super_admin,admin');
    }

    public function dataTable(RoleDataTable $table)
    {

         return respondWithJson(true,$table->build()->original,'',200);


    }

    public function store(
        CreateRequest $request,
        Create $create
    )
    {


        $result= $create->execute($request);


         return respondWithJson(true,$result,'Rol registrado!',200);
     
    }

 

    public function update(
        UpdateRequest $request,
        Update $update,
        $id
    )
    {

            $request->merge(["role_id"=>$id]);

            $result= $update->execute($request);


         return respondWithJson(true,$result,'Rol modificado!',200);


      
    }


    public function show(
        $id,
        Show $show
    )
    {

         $model = $show->execute($id);


        return respondWithJson(true,$model,'',200);

    }

   
     public function destroy(
        Destroy $destroy,
         $id
    )
    {

        $destroy->execute($id);
        
        return respondWithJson(true,[],'Rol eliminado!',200);

      
    }

      public function destroy_multiple(
        Destroy $destroy,
        DestroyRequest $request
    )
    {

        $destroy->destroy_multiple($request);
        
        return respondWithJson(true,[],'Roles eliminados!',200);

      
    }
  
}
