<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
    	'user_id',
    	'ima_profile'
    ];

     protected $hidden = [
        'pivot'
    ];

    protected $table="profiles";

}
