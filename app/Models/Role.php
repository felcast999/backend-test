<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{    
    use SoftDeletes;
    
    protected $fillable = [
    	'name',
    	'display_name',
    	'description'
    ];

     protected $hidden = [
        'pivot'
    ];


    /* relationships */ 

    public function users()
    {
    	return $this->belongsToMany(User::class);
    }

}
