<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
     use HasFactory, Notifiable,SoftDeletes,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'profile_pic',
        'status_id',
        'phone',
        'description'
    ];

    protected $relations = [
        'roles:id,name,display_name'
    ];


    protected static $relations_to_cascade = ['images']; 

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','pivot'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    protected $appends = [
        'image',
    ];
    // Rest omitted for brevity

       /**
        * Get the identifier that will be stored in the subject claim of the JWT.
        *
        * @return mixed
        */

       /**
        * Return a key value array, containing any custom claims to be added to the JWT.
        *
        * @return array
        */



    protected static function boot()
    {
        parent::boot();

        static::deleting(function($resource) {
            foreach (static::$relations_to_cascade as $relation) {
                foreach ($resource->{$relation}()->get() as $item) {
                    $item->delete();
                }
            }
        });

        static::restoring(function($resource) {
            foreach (static::$relations_to_cascade as $relation) {
                foreach ($resource->{$relation}()->get() as $item) {
                    $item->withTrashed()->restore();
                }
            }
        });
    }

    public function getImageAttribute()
    {
        if (!empty($this->images()->first())) 
        {
           return $this->images()->latest()->first()->ima_profile;
        }

        return null;
    }

    /* accessors */

    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = ucwords(strtolower($value));
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = ucwords(strtolower($value));
    }

  
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function setProfilePicAttribute($value)
    {
        if (is_null($value)) 
            $this->attributes['profile_pic'] = "images/pictures/default.png"; 
        else
            $this->attributes['profile_pic'] = $value; 
    }

    /* relationships */

   


    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }


    public function status()
    {
        return $this->belongsTo(Status::class,'status_id');
    }




 

    /* methods */

    public function withRelationships()
    {
       $this->removeLoadedRelations();
      return $this->load($this->getRelations());
    }

    public function removeLoadedRelations()
    {
        foreach ($this->getRelations() as $key => $value)
        {
            /*The relations where his key is not a integer they're loaded relations*/
            if (!is_int($key) && $this->relationLoaded($key))
                $this->unsetRelation($key);
        }
    }

    public function isAdmin()
    {
        return $this->hasOneOfTheseRoles('admin');
    }

    public function hasOneOfTheseRoles(...$roles)
    {


        if ($this->hasRoles())
        {
             $required_roles = is_array($roles[0]) ? 
            collect($roles)->collapse() :
            collect(func_get_args());

            $rolesOfThisUser = self::getRolesOfThisUser();

            foreach ($required_roles as $required_role)
            {
                if ($rolesOfThisUser->contains($required_role))
                    return true;
            }

            return false;
        }

        return false;
     }

     public function hasRoles()
     {
         return $this->roles->isNotEmpty();
     } 

    private function getRolesOfThisUser()
    {
        $role_names = collect();

        $this->roles->each(function($role) use($role_names){ 
            $role_names->push($role->name); 
        });

        return $role_names;
    }


        //scopes

    public function scopeExclude($query, $value = []) 
{
    return $query->select(array_diff($this->columns, (array) $value));
}


    public function images()
    {

     return $this->hasMany(Profile::class, 'user_id', 'id');

    }


}
