<?php

namespace App\UseCases\V1\User;

use App\Models\User;
use App\Models\Profile;
use App\Models\Role;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Carbon;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Create
{
    private $user;
    private $role;
    private $profile;


    public function __construct(
        User $user,
        Role $role,
        Profile $profile,

    )
    {
        $this->user = $user;
        $this->role = $role;
        $this->profile = $profile;

    }

    public function execute(Request $request)
    {
        

     

        $user = $this->user->create($request->all());

        if($request->file('avatar')) 
        {
            $fileName = time().'_'.$request->file('avatar')->getClientOriginalName();
            $filePath = $request->file('avatar')->storeAs('avatar/'.$user->id, $fileName, 'public');
            #$filePath = $request->file('avatar')->storeAs('avatar/'.$user->id, $fileName, 's3');

            #$url = '/storage/' . $filePath;
            $url = $filePath;

        #$user->avatar=Storage::disk('public')->url($filePath);
        #$user->avatar=Storage::disk('s3')->url($filePath);

        $this->profile->create(["user_id"=>$user->id,"ima_profile"=>Storage::disk('public')->url($filePath)]);
 
        }



        $user->save();

        $user->roles()->attach($request->role_id);
  
        return $user;

      
    }


   
}