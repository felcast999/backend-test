<?php

namespace App\UseCases\V1\User;

use App\Models\User;
use Illuminate\Http\Request;

class Destroy
{
    private $model;

    public function __construct(
        User $model
    )
    {
        $this->model = $model;
    }

    public function execute($id)
    {
         $this->model->find($id)->images()->delete();

        return  $this->model->destroy($id);
    }


      public function destroy_multiple(Request $request)
    {

        foreach ($request->ids as $key => $value) {
            $this->model->find($value)->images()->delete();

        }

        return  $this->model->whereIn('id',$request->ids)->delete();
    }
}