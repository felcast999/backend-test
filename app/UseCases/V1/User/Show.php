<?php

namespace App\UseCases\V1\User;

use App\Models\User;

class Show
{
    private $model;

    public function __construct(
        User $model
    )
    {
        $this->model = $model;
    }

    public function execute($id)
    {

        
        $user = $this->model->findOrFail($id);
        
        $user->load('roles');
        
        return compact('user');
    }
}