<?php

namespace App\UseCases\V1\User;

use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Carbon;
use App\Models\Profile;

class Update
{
    private $model;
    private $profile;


    public function __construct(
        Profile $profile,
        User $model

    )
    {
        $this->model = $model;
        $this->profile = $profile;

    }

    public function execute(Request $request)
    {
        $model = $this->model->find($request->user_id);


        $model->first_name=$request->first_name;
        $model->last_name=$request->last_name;
        $model->email=$request->email;
        $model->phone=$request->phone;
        $model->status_id=$request->status_id;



         if($request->file('avatar')) 
        {



  if (!empty($request->file('avatar')->getClientOriginalName())) 
            {

            $model->images()->delete();

            $fileName = time().'_'.$request->file('avatar')->getClientOriginalName();
            $filePath = $request->file('avatar')->storeAs('avatar/'.$model->id, $fileName, 'public');

            #$url = '/storage/' . $filePath;
            $url = $filePath;

           # $model->avatar=Storage::disk('s3')->url($filePath);
      
            $this->profile->create(["user_id"=>$model->id,"ima_profile"=>Storage::disk('public')->url($filePath)]);

             }

       


        }



 if ($request->change_password) 
        {
               $model->password=$request->password;

        }
        #$model->update($request->all());
        $model->save();



        $model->roles()->sync($request->role_id);
       
        return $model;
    }


   
}