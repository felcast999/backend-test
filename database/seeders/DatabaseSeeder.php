<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");

        $this->call(StatusSeeder::class);

        $this->call(UserSeeder::class);
        $this->call(RoleSeeder::class);
  



        DB::statement("SET FOREIGN_KEY_CHECKS = 1");
    }
}
