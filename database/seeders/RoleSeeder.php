<?php

namespace Database\Seeders;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        
        $roles = [
            'super_admin'=>'super administrador',
        	'admin' => 'adminisdrator',
        	'system_user' => 'usuario de sistema',
        	'warehouse_manager' => 'jefe de almacen',
            'administrative_assistant' => 'asistente de administracion',
            'administration_manager' => 'encargado de administracion',
            'accounting_assistant' => 'asistente de contabilidad',
            'treasury_assistantr' => 'asistente de tesoreria',
            'head_of_Treasury' => 'jefe de tesoreria',
            'construction_manager' => 'administrador de obtra',

        ];


        foreach ($roles as $name => $description)
        {
        	$role = Role::create([
		        		'name' => $name,
		        		'display_name' => $name,
		        		'description' => $description
		        	]);

        }
        
        $admin_role = Role::where('name','admin')->first();
        $super_admin_role = Role::where('name','super_admin')->first();

        // admin
        $admin = User::whereEmail('admin@finance.com')->first();
        $admin->roles()->attach([$admin_role->id, $super_admin_role->id]);

        
    }
}
