<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

  <div>
    <div class="h-full  bg-gray-300 ">
        <div class="grid grid-cols-12 gap-0">
            <div class="col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-12 xxl:col-span-12 px-6 py-12">
                <div class="grid grid-cols-12">
                  
                    <div class="col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-12 xxl:col-span-12">
                        <!-- Start Card List -->
                        <div class=" bg-white  p-3 rounded-xl  shadow-xl flex items-center grid grid-cols-3 gap-4">


                            <div class="flex space-x-6 items-center col-start-1 col-end-1 ...">
                                 
                                <div>
                                <img src="https://www.nicepng.com/png/detail/395-3955418_paypal-icon-png.png" class="inline object-cover w-16 h-16 mr-2 rounded-full"/>
  
                                </div>

                                <div>
                                    <p class="font-semibold text-base">Maria Carrillo</p>
                                    <p class="font-semibold text-xs text-gray-400">Eliminar</p>
                                </div>              
                            </div>
                              <div class="col-start-2 col-end-6 ...">
                                <div class="bg-yellow-200 rounded-md p-12 flex items-center">
                                    <p class="text-yellow-600 font-semibold text-xs">-C4,678</p>
                                </div>
                            </div>  
                            <div class="col-start-6 col-end-7 ... ">
                                <div class="bg-yellow-200 rounded-md p-12 flex items-center">
                                    <p class="text-yellow-600 font-semibold text-xs">-C4,678</p>
                                </div>
                            </div>    
                        </div>
                        <!-- End Card List -->
                    </div>


            </div>
        </div>
    </div>
  </div>
</div>
</x-app-layout>
