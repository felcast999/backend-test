<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('me', 'AuthController@me');
    });



});


Route::group([
	'prefix' => 'v1',
	'namespace' => 'api\v1',
	'middleware' => 'auth:api'
], function(){



  Route::post('user-profile', 'admin\UserController@update_profile');

	 Route::resources([

      // usuarios
     'users'=>'admin\UserController',

     //roles
     'roles'=>'admin\RoleController',

    ]);






  // destroy


   Route::post('users-destroy-multiple','admin\UserController@destroy_multiple');
   Route::post('roles-destroy-multiple','admin\RoleController@destroy_multiple');
   


});