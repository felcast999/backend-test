<?php

use Illuminate\Support\Facades\Route;

Route::group([
	'prefix'  	=> 'v1/datatables',
	'middleware' => ['auth:api'],
	'namespace' => 'api\v1'
], function(){


//users

Route::post('/users','admin\UserController@dataTable');


// roles

Route::post('/roles','admin\RoleController@dataTable');



});